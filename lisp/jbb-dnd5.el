;;; des fonctions pour D&D5

;;; Système de création de tables aléatoires
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Usage :
;; (jbrpg-roll-weighted-list `((,(format "%s kobolds" (jbrpg-roll-dice 6 2)) . 20)
;;  			    ("1 troll" . 15)
;;  			    (,(format "%s orcs" (jbrpg-roll-dice 6 3)) . 65)))

(defun jbrpg-roll-weighted-list (alist)
  "Transforms a weighted `alist' of format (item . odds) into a `pcase' sexp and evals it.
The total odds will be a sum of all the `odds' values in the `alist'."
  (eval (cl-loop for (key . value) in alist
                 for running-total = (+ (or running-total 1) value)
                 collect `((pred (> ,running-total)) ,key) into list
                 finally return (append `(pcase (1+ (random ,(1- running-total))))
                                        list))))
;;; jeter des dés, c'est inveré → 2d6 = 6 2
(defun jbrpg-roll-dice (&optional dice num &rest exclude)
  "Roll a random number from 1 to `dice' (default 6).
If `num' is specified, roll `num' times and sum the result.
If `exclude' numbers are specified, re-roll any matching results (after summing)."
  (let* ((dice (or dice 6))
         (num (or num 1))
         (output (cl-loop repeat num
                          sum (1+ (random dice)))))
    (if (member output exclude)
        (apply #'jbrpg-roll-dice dice num exclude)
      output)))
;;; fonction à utiliser quand il y a un nombre aléatoire de monstres (avec des pv aléatoires) :
;;; exemple 2d4 méphites vaporeux :
;;; (,(format "%s" (jbdomon 4 2 'mns-mephite-vaporeux)) . 10)
(defun jbdomon (dice num name)
  (let ((result ""))
    (dotimes (i (jbrpg-roll-dice dice num) result)
      (setq result (concat result (jbltm name)))
      )))
;;; TABLES DE RENCONTRES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Variables des monstres CA PV Deg mod p.
(defun jbltm (monster)
  (let (
	(mns-elementaire-eau (format "Élémentaire_de_l'eau 14 %s 0 +2 p.120\n" (+ (jbrpg-roll-dice 10 12) 48)))
	(mns-guenaude-marine (format "Guenaude_aquatique 14 %s 0 +1 p.176\n" (+ (jbrpg-roll-dice 8 7) 21)))
	(mns-hippocampe-geant (format "Hippocampe_géant 13 %s 0 +2 p.328\n" (jbrpg-roll-dice 10 3)))
	(mns-mephite-vaporeux (format "Méphite_vaporeux 10 %s 0 +0 p.219\n" (jbrpg-roll-dice 6 6)))
	(mns-merrow (format "Merrow 13 %s 0 +0 p.220\n" (+ (jbrpg-roll-dice 10 6) 12)))
	(mns-orque-epaulard (format "Épaulard 12 %s 0 +0 p.327\n" (+ (jbrpg-roll-dice 12 12) 12)))
	(mns-pieuvre-geante (format "Pieuvre_géante 11 %s 0 +1 p.335\n" (+ (jbrpg-roll-dice 10 8) 8)))
	(mns-piranha (format "Piranha 13 %s 0 +3 p.336\n" (- (jbrpg-roll-dice 4 1) 1)))
	(mns-plesiosaure (format "Plésiosaure 13 %s 0 +2 p.76\n" (+ (jbrpg-roll-dice 10 8) 24)))
	(mns-requin-chasseur (format "Requin-chasseur 12 %s 0 +1 p.337\n" (+ (jbrpg-roll-dice 10 6) 12)))
	(mns-requin-geant (format "Requin_géant 13 %s 0 +0 p.338\n" (+ (jbrpg-roll-dice 12 11) 55)))
	(mns-requin-recif (format "Requin_de_récif 12 %s 0 +1 p.337\n" (+ (jbrpg-roll-dice 8 4) 4)))
	(mns-sahuagin (format "Sahuagin 12 %s 0 +0 p.259\n" (+ (jbrpg-roll-dice 8 4) 4)))
	(mns-sahuagin-baron (format "Baron_sahuagin 16 %s 0 +2 p.260\n" (+ (jbrpg-roll-dice 10 9) 27)))
	(mns-sahuagin-pretresse (format "Prêtresse_sahuagin 12 %s 0 +0 p.260\n" (+ (jbrpg-roll-dice 8 6) 6)))
	(mns-serpent-constricteur (format "Serpent_constricteur 12 %s 0 +2 p.339\n" (+ (jbrpg-roll-dice 10 2) 2)))
	(mns-serpent-constricteur-geant (format "Serpent_constricteur_géant 12 %s 0 +2 p.339\n" (+ (jbrpg-roll-dice 12 8) 8)))
	(mns-thalasseen (format "Homme_poisson 11 %s 0 +1 p.187\n" (+ (jbrpg-roll-dice 8 2) 2)))
	)
    (print (eval monster))))

;; (defun let-the-monster (lemonstre)
;;   (let ((mns-def-test (format "||Monstre|+2|13|%s|0||p.666|" (jbrpg-roll-dice 6 3)))
;;       (mns-def-test2 (format "||Monstre2|+3|13|%s|0||p.666|" (jbrpg-roll-dice 6 3))))
;;     (print (eval lemonstre))))
;; (message "%s" (let-the-monster 'mns-def-test))

;;; Rencontres aquatiques
;;niveau 1 à 4
(defun dd5-rencontres-aquatiques-1-4 ()
  (interactive)
  (insert (jbrpg-roll-weighted-list `(
			      (,(format "%s" (jbdomon 6 3 'mns-piranha)) . 10)
 			      (,(format "%s" (jbdomon 4 2 'mns-mephite-vaporeux)) . 4)
			      (,(format "%s" (jbdomon 4 1 'mns-sahuagin)) . 4)
			      (,(format "%s cadavres de marins noyés, enchevêtrés dans les algues" (jbrpg-roll-dice 4 2)) . 3)
			      (,(format "%s" (jbdomon 4 2 'mns-serpent-constricteur)) . 3)
			      (,(format "%s" (jbdomon 4 1 'mns-requin-recif)) . 4)
			      ("Une nuée de piranhas" . 4)
			      ("Un tapis d'énormes palourdes" . 3)
			      (,(format "%s%s" (jbdomon 10 1 'mns-thalasseen) (jbdomon 3 1 'mns-hippocampe-geant)) . 5)
			      (,(format "%s" (jbltm 'mns-pieuvre-geante)) . 5)
			      (,(format "%s" (jbltm 'mns-merrow)) . 5)
			      (,(format "%s" (jbltm 'mns-plesiosaure)) . 5)
			      (,(format "%s pièces de vaisselle en airain corrodées, éparpillées au fond de l'eau" (jbrpg-roll-dice 10 2)) . 5)
			      (,(format "%s" (jbltm 'mns-serpent-constricteur-geant)) . 5)
			      (,(format "%s" (jbltm 'mns-guenaude-marine)) . 5)
			      (,(format "Un banc de poissons argentés qui filent sous l'eau") . 5)
			      (,(format "%s" (jbdomon 4 1 'mns-requin-chasseur)) . 5)
			      (,(format "%s%s" (jbltm 'mns-sahuagin-pretresse) (jbdomon 4 2 'mns-hippocampe-geant)) . 5)
			      (,(format "%s" (jbdomon 4 1 'mns-orque-epaulard)) . 6)
			      (,(format "%s" (jbltm 'mns-requin-geant)) . 2)
			      (,(format "%s" (jbltm 'mns-elementaire-eau)) . 1)
			      (,(format "%s" (jbltm 'mns-sahuagin-baron)) . 1)
			      ))))

;; TODO : créer un générateur de boutique :
;; → liste d'objets avec prix aléatoire (dans une tranche 30 à 50 PO etc.)
;;   fonction du genre (boutique commun peu_commun rare legendaire), arme, armure, potions etc.


