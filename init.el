;; jbbourgoin emacs configuration 2021

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; BASES

;; Pour les configurations spécifiques à Termux.
;; Usage : (when termux-p ... )
(defvar termux-p
  (not (null (getenv "ANDROID_ROOT")))
  "If non-nil, GNU Emacs is running on Termux.")
;; Pour le reste
(load "~/.emacs.d/os-variables.el")
(add-to-list 'load-path "~/.emacs.d/lisp/")

;; packages management
(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))
(add-to-list 'package-archives '("nongnu" . "https://elpa.nongnu.org/nongnu/"))
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))

;; straight.el
;; Install straight.el
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("aa6638f0cd2ba2c68be03220ea73495116dc6f0b625405ede34087c1babb71ae" "c4063322b5011829f7fdd7509979b5823e8eea2abf1fe5572ec4b7af1dd78519" "3d4df186126c347e002c8366d32016948068d2e9198c496093a96775cc3b3eaa" "d89e15a34261019eec9072575d8a924185c27d3da64899905f8548cbd9491a36" "a0be7a38e2de974d1598cf247f607d5c1841dbcef1ccd97cded8bea95a7c7639" "e8df30cd7fb42e56a4efc585540a2e63b0c6eeb9f4dc053373e05d774332fc13" "234dbb732ef054b109a9e5ee5b499632c63cc24f7c2383a849815dacc1727cb6" "e2c926ced58e48afc87f4415af9b7f7b58e62ec792659fcb626e8cba674d2065" "6c98bc9f39e8f8fd6da5b9c74a624cbb3782b4be8abae8fd84cbc43053d7c175" "846b3dc12d774794861d81d7d2dcdb9645f82423565bfb4dad01204fa322dbd5" "6c531d6c3dbc344045af7829a3a20a09929e6c41d7a7278963f7d3215139f6a7" "7a7b1d475b42c1a0b61f3b1d1225dd249ffa1abb1b7f726aec59ac7ca3bf4dae" "1d5e33500bc9548f800f9e248b57d1b2a9ecde79cb40c0b1398dec51ee820daf" "a9a67b318b7417adbedaab02f05fa679973e9718d9d26075c6235b1f0db703c8" "8146edab0de2007a99a2361041015331af706e7907de9d6a330a3493a541e5a6" "1704976a1797342a1b4ea7a75bdbb3be1569f4619134341bd5a4c1cfb16abad4" "835868dcd17131ba8b9619d14c67c127aa18b90a82438c8613586331129dda63" "4f1d2476c290eaa5d9ab9d13b60f2c0f1c8fa7703596fa91b235db7f99a9441b" "efcecf09905ff85a7c80025551c657299a4d18c5fcfedd3b2f2b6287e4edd659" "4c56af497ddf0e30f65a7232a8ee21b3d62a8c332c6b268c81e9ea99b11da0d3" "84d2f9eeb3f82d619ca4bfffe5f157282f4779732f48a5ac1484d94d5ff5b279" default))
 '(exwm-floating-border-color "#504945")
 '(helm-completion-style 'emacs)
 '(jdee-db-active-breakpoint-face-colors (cons "#0d1011" "#fabd2f"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#0d1011" "#b8bb26"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#0d1011" "#928374"))
 '(objed-cursor-color "#fb4934")
 '(package-selected-packages
   '(harpoon fill-column-indicator good-scroll decide autumn-light-theme fira-code-mode w3m marginalia citeproc with-editor magit selectrum-prescient embark-consult embark orderless selectrum writeroom-mode gotham-theme green-screen-theme green-phosphor-theme green-is-the-new-black-theme tron-legacy-theme mixed-pitch linum-off olivetti org-superstar org-appear org org-roam counsel solarized-theme outline-minor-faces bicycle vterm exwm-surf backlight xbacklight hydra pulseaudio-control unfill dired-single dired-x exwm flycheck-grammalecte markdown-mode todotxt-mode helm-projectile helm flycheck company crux which-key expand-region smartparens diminish smart-mode-line-powerline-theme doom-themes use-package))
 '(pdf-view-midnight-colors (cons "#ebdbb2" "#282828"))
 '(rustic-ansi-faces
   ["#282828" "#fb4934" "#b8bb26" "#fabd2f" "#83a598" "#cc241d" "#8ec07c" "#ebdbb2"])
 '(safe-local-variable-values
   '((org-d20-party
      ("Astar" . 2)
      ("Falrim" . 2)
      ("Melissande" . 3)
      ("Quarion" . 5))))
 '(warning-suppress-types '((comp) (comp) (comp) (comp) (comp))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Hack" :foundry "SRC" :slant normal :weight normal :height 120 :width normal)))))

;; temp files
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

;; qol
(fset 'yes-or-no-p 'y-or-n-p)

;; dépendances diverses :
(use-package hydra
  :ensure t
  :config)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; GUI

;; visuals
(tool-bar-mode -1)
(blink-cursor-mode -1)
;; hl-line
(global-hl-line-mode 0)
;; LIN (remplacement hl-line)
;; https://gitlab.com/protesilaos/lin
(add-to-list 'load-path "~/.emacs.d/lisp/lin")
(require 'lin)
(setq lin-face 'lin-blue-override-fg)
(lin-setup)
;; pulsar : highlight line
(use-package pulsar
  :ensure t
  :config
  (setq pulsar-pulse t)
  (setq pulsar-delay 0.055)
  (setq pulsar-iterations 10)
  (setq pulsar-face 'pulsar-magenta)
  (setq pulsar-highlight-face 'pulsar-yellow)
  (pulsar-global-mode 1))
;;(line-number-mode 1)
;;(global-display-line-numbers-mode 1)
(column-number-mode t)
(size-indication-mode t)
(setq inhibit-startup-screen t)
(setq-default word-wrap t)
(when linuxgui-p
  (menu-bar-mode -1)
  (toggle-scroll-bar -1))
;; ajout d'une barre verticale à 80 char
;;(setq-default display-fill-column-indicator-column 79)
;;(add-hook 'org-mode-hook #'display-fill-column-indicator-mode)

;; désactiver l'affichage des numéros de ligne dans certains modes
;; (use-package linum-off
;;   :ensure t
;;   :config
;;   (setq linum-disabled-modes-list
;; 	'(vterm-mode eshell-mode shell-mode term-mode ansi-term-mode org-mode)))

;; ;; scroll fix
;; (setq scroll-margin 0
;;       scroll-conservatively 100000
;;       scroll-preserve-screen-position 1)

;; (use-package good-scroll
;;   :ensure t
;;   :config
;;   (good-scroll-mode 1))

;; theme
;; solarized-light, solarized-selenized-white
;; (use-package solarized-theme :ensure t :config
;;  (load-theme 'solarized-dark t))

;; (use-package tron-legacy-theme :ensure t :config
;;   (load-theme 'tron-legacy t))

;; (use-package gotham-theme :ensure t :config
;;   (load-theme 'gotham t))

(use-package gotham-theme :ensure t :config
  (load-theme 'doom-dracula t))

;; (use-package green-is-the-new-black-theme :ensure t :config
;;     (load-theme 'green-is-the-new-black t))

;; (use-package green-phosphor-theme :ensure t :config
;;   (load-theme 'green-phosphor t))

;; (use-package green-screen-theme :ensure t :config
;;   (load-theme 'green-screen t))

;; (use-package autumn-light-theme :ensure t :config
;;    (load-theme 'autumn-light t))

;; doom-gruvbox with correction for graphite theme
;; (load-theme 'doom-gruvbox t)
;;(add-to-list 'default-frame-alist '(background-color . "#2C2C2C"))
;; (defun set-background-for-graphite (&optional frame)
;;   (or frame (setq frame (selected-frame)))
;;   (set-face-background 'default "#2c2c2c" frame))
;; (add-hook 'after-make-frame-functions 'set-background-for-graphite)
;; (add-hook 'window-setup-hook 'set-background-for-graphite)


;; grey, black, cream, white
;; (use-package almost-mono-themes :ensure t :config
;;     (load-theme 'almost-mono-white t))

;; (use-package arc-dark-theme
;;   :straight (:host github :repo "cfraz89/arc-dark-theme")
;;   :config (load-theme 'arc-dark t))


;; police de caractère
;; Go Mono - Hack - Hasklig - Fantasque Sans Mono - monofur
;; Space Mono - SF Mono - Monoid - Fira Code (ligature)

(when linuxgui-p
  (set-frame-font "SF Mono-12" nil t))

;; smart parens
;; (use-package smartparens
;;   :ensure t
;;   :diminish smartparens-mode
;;   :config
;;   (progn
;;     (require 'smartparens-config)
;;     (smartparens-global-mode 1)
;;     (show-paren-mode t)))

;; (use-package paren
;;   :config
;;   (setq show-paren-style 'expression)
;;   (setq show-paren-when-point-in-periphery t)
;;   (setq show-paren-when-point-inside-paren nil)
;;   :hook (after-init-hook . show-paren-mode))

;; region select
(use-package expand-region
  :ensure t
  :bind ("M-m" . er/expand-region))

;;; MODE LINE

;; frame title format show full path
(setq frame-title-format
      '((:eval (if (buffer-file-name)
       (abbreviate-file-name (buffer-file-name))
       "%b"))))


;; smart mode & powerline
(use-package smart-mode-line-powerline-theme
 :ensure t)

(use-package smart-mode-line
  :ensure t
  :config
  (setq sml/theme 'powerline)
  (add-hook 'after-init-hook 'sml/setup))

;; no minor mode in title frame
(use-package diminish
  :ensure t)

(setq display-time-24hr-format t)
(display-time)
(when linuxgui-p (display-battery-mode 1))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; KEYBINDS & AUTOCOMPLETE

;; keystroke helper
(use-package which-key
  :ensure t
  :diminish which-key-mode
  :config
  (which-key-mode +1))

;; keystroke improvements based on prelude
(use-package crux
  :ensure t
  :bind
  ("C-k" . crux-smart-kill-line)
  ("C-c n" . crux-cleanup-buffer-or-region)
  ("C-c f" . crux-recentf-find-file)
  ("C-a" . crux-move-beginning-of-line))

;; autocomplete
(use-package company
  :ensure t
  :diminish company-mode
  :config
  (add-hook 'after-init-hook #'global-company-mode))

(use-package flycheck
  :ensure t
  :diminish flycheck-mode
  :config
  (add-hook 'after-init-hook #'global-flycheck-mode))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; WINDOWS

(windmove-default-keybindings)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; EDITION

;; unfill
(use-package unfill
  :ensure t
  :config
  (define-key global-map "\M-Q" 'toggle-fill-unfill))

;; markdown

(use-package markdown-mode
  :ensure t
  :config)

;; correction grammaticale grammalecte
(use-package flycheck :ensure t)
(use-package flycheck-grammalecte
  :ensure t
  :hook (fountain-mode . flycheck-mode)
  :init
  (setq flycheck-grammalecte-report-spellcheck nil
	flycheck-grammalecte-report-grammar t
	flycheck-grammalecte-report-apos t
	flycheck-grammalecte-report-esp t
	flycheck-grammalecte-report-nbsp t)
  :config
  (add-to-list 'flycheck-grammalecte-enabled-modes 'fountain-mode)
  (grammalecte-download-grammalecte)
  (with-eval-after-load 'flycheck
    (flycheck-grammalecte-setup)))

;;; Flyspell

(setq ispell-program-name "aspell")
(setq ispell-local-dictionary "fr_FR")
(global-set-key [f2] 'flyspell-mode)
;;(dolist (hook '(org-mode-hook))
;;  (add-hook hook (lambda () (flyspell-mode 1))))

;;; EPUB READER
(use-package nov :ensure t :config)
(add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))
;; police spécifique
;; (defun my-nov-font-setup ()
;;   (face-remap-add-relative 'variable-pitch :family "Liberation Serif"
;;                                            :height 1.0))
;; (add-hook 'nov-mode-hook 'my-nov-font-setup)

;;; Images use external converter
(setq image-use-external-converter t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; ORG

(use-package org :ensure t :config)

;; improve org-mode looks
(setq org-startup-indented t
      org-pretty-entities nil
      org-hide-emphasis-markers t
      org-startup-with-inline-images t
      org-image-actual-width '(300))
(add-hook 'org-mode-hook #'visual-line-mode)
(use-package org-appear
  :ensure t
  :hook (org-mode . org-appear-mode)
  :config
  (setq org-appear-autosubmarkers t))

;; nice bullets
(use-package org-superstar
  :ensure t
  :config
  (setq org-superstar-special-todo-items t
	inhibit-compacting-font-caches t)
  (add-hook 'org-mode-hook (lambda ()
			     (org-superstar-mode 1))))

;; line spacing
(setq-default line-spacing 2)

;; darkroom distraction-free-mode bind to F8 key, fullscreen to f9
(defun toggle-fullscreen ()
  "Toggle full screen on X11"
  (interactive)
  (when (eq window-system 'x)
    (set-frame-parameter
     nil 'fullscreen
     (when (not (frame-parameter nil 'fullscreen)) 'fullboth))))
(global-set-key [f9] 'toggle-fullscreen)

;; org2epub

(use-package ox-epub
  :ensure t
  :config )

;;; APPLICATIONS
(add-to-list 'org-file-apps '("\\.pdf\\'" . "evince %s"))

(use-package darkroom
  :ensure t
  :config
  (setq darkroom-text-scale-increase 1.5)
  :bind
  (("<f8>" . darkroom-tentative-mode)))

;; Des lettres avec KOMA
(eval-after-load 'ox '(require 'ox-koma-letter))

;; mindmap avec org-mind-map
;; This is an Emacs package that creates graphviz directed graphs from
;; the headings of an org file
(use-package org-mind-map
  :init
  (require 'ox-org)
  :ensure t
  ;; Uncomment the below if 'ensure-system-packages` is installed
  ;;:ensure-system-package (gvgen . graphviz)
  :config
  (setq org-mind-map-engine "dot")       ; Default. Directed Graph
  ;; (setq org-mind-map-engine "neato")  ; Undirected Spring Graph
  ;; (setq org-mind-map-engine "twopi")  ; Radial Layout
  ;; (setq org-mind-map-engine "fdp")    ; Undirected Spring Force-Directed
  ;; (setq org-mind-map-engine "sfdp")   ; Multiscale version of fdp for the layout of large graphs
  ;; (setq org-mind-map-engine "twopi")  ; Radial layouts
  ;; (setq org-mind-map-engine "circo")  ; Circular Layout
  )

;; Auto-Fill
;;(add-hook 'org-mode-hook 'turn-on-auto-fill)

;;; Gestion des biblios
;; (use-package org-ref
;;   :ensure t
;;   :config
;;   (require 'org-ref)
;;   (setq reftex-default-bibliography '("~/ecrits-prives/Biblio/biblio.bib"))
;;   (setq org-ref-bibliography-notes "~/ecrits-prives/Biblio/biblio.org"
;;       org-ref-default-bibliography '("~/ecrits-prives/Biblio/biblio.bib")
;;       org-ref-pdf-directory "~/ecrits-prives/Biblio/biblio-pdf"))

(use-package citeproc
  :ensure t
  :config)
;;(setq org-cite-global-bibliography "~/ecrits-prives/Biblio/biblio.bib")
;;(setq org-cite-csl-styles-dir "~/ecrits-prives/Biblio/styles")
(use-package oc-csl)


;;; DND 5 tools
(load "~/.emacs.d/lisp/jbb-dnd5.el")
(load "~/.emacs.d/lisp/org-lookup-dnd/org-lookup-dnd.el")
(use-package org-d20
  :ensure t
  :config)


;;; mermaid, pour faire des diagrammes
(use-package ob-mermaid
  :ensure t
  :config
  (setq ob-mermaid-cli-path "~/node_modules/.bin/mmdc"))

(org-babel-do-load-languages
    'org-babel-load-languages
    '((mermaid . t)
      (scheme . t)))


;;; IVY
;; (use-package counsel
;;   :ensure t
;;   :config
;;   (ivy-mode 1)
;;   (counsel-mode 1))

;; (global-set-key (kbd "s-SPC") 'counsel-linux-app)

;;; SMOCE ( Selectrum Marginalia Orderless Counsult Embark)
;; Selectrum : Easily select item from list
;; Marginalia : Enrich existing commands with completion annotations
;; Orderless : Completion style for matching regexps in any order
;; Counsult : 
;; Embark : Conveniently act on minibuffer completions

;;; Selectrum
(use-package vertico
  :ensure t
  :config
  (vertico-mode +1))

;;; Marginalia
(use-package marginalia
  :ensure t
  ;; Either bind `marginalia-cycle` globally or only in the minibuffer
  :bind (("M-A" . marginalia-cycle)
         :map minibuffer-local-map
         ("M-A" . marginalia-cycle))
  ;; The :init configuration is always executed (Not lazy!)
  :init
  ;; Must be in the :init section of use-package such that the mode gets
  ;; enabled right away. Note that this forces loading the package.
  (marginalia-mode))

;;; Orderless
(use-package orderless
  :ensure t
  :custom
  (completion-styles '(orderless))
  (setq selectrum-refine-candidates-function #'orderless-filter)
  (setq selectrum-highlight-candidates-function #'orderless-highlight-matches)
  )

;;; Consult
(use-package consult
  :ensure t
  :config)

;;; Embark
(use-package embark
  :ensure t
  :bind
  (("C-." . embark-act)         ;; pick some comfortable binding
   ("C-;" . embark-dwim)        ;; good alternative: M-.
   ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'
  :init
  ;; Optionally replace the key help with a completing-read interface
  (setq prefix-help-command #'embark-prefix-help-command)
  :config
  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none)))))

(use-package embark-consult
  :ensure t
  :after (embark consult)
  :demand t ; only necessary if you have the hook below
  ;; if you want to have consult previews as you move around an
  ;; auto-updating embark collect buffer
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; DIRED

;; Dired-x
(use-package dired-x
  :config
  (progn
    (setq dired-omit-verbose nil)
    ;; hide backup, autosave, *.*~ files
    ;; omit mode can be toggled using `M-o' in dired buffer
    (add-hook 'dired-mode-hook #'dired-omit-mode)))
;; Dired-single
(use-package dired-single
  :ensure t
  :bind (:map modi-mode-map
         ;; Change the default `C-x C-d` key binding from `ido-list-directory'
         ("C-x C-d" . dired-single-magic-buffer-current-dir)
         ;; Change the default `C-x C-j` key binding from `dired-jump'
         ;; Opens dired-single-magic-buffer but asks which directory to open that
         ;; dired buffer for.
         ("C-x C-j" . dired-single-magic-buffer))
  :config
  (progn
    (defun dired-single-magic-buffer-current-dir ()
      "Open a single magic dired buffer for the current buffer directory."
      (interactive)
      (dired-single-magic-buffer default-directory))

    (defun dired-single-up-directory ()
      (interactive)
      (dired-single-buffer ".."))

    (with-eval-after-load 'dired
      (bind-keys
       :map dired-mode-map
       ("<return>"         . dired-single-buffer)
       ("<double-mouse-1>" . dired-single-buffer-mouse)
       ("^"                . dired-single-up-directory)))))

;; Dired dans un seul buffer
(define-key dired-mode-map (kbd "RET") 'dired-find-alternate-file) ; was dired-advertised-find-file
(define-key dired-mode-map (kbd "^") (lambda () (interactive) (find-alternate-file "..")))  ; was dired-up-directory

;; DIRVISH
(straight-use-package 'dirvish)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; BOOKMARK

;; (use-package harpoon
;;   :ensure t
;;   :config)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SCHEME

;; tinyscheme sous Termux
(when termux-p
  (setq scheme-program-name "tinyscheme"))

(when linuxgui-p
  (setq scheme-program-name "guile"))

;;; W3M

;; (use-package w3m
;;   :ensure t
;;   :config )

;;; Reddit client
;; (use-package md4rd :ensure t
;;   :config
;;   (add-hook 'md4rd-mode-hook 'md4rd-indent-all-the-lines)
;;   ;;(setq md4rd-subs-active '(emacs ))
;;   ;;(setq md4rd--oauth-access-token "your-access-token-here")
;;   ;;(setq md4rd--oauth-refresh-token "your-refresh-token-here")
;;   (run-with-timer 0 3540 'md4rd-refresh-login))

(require 'server)
(if (not (server-running-p)) (server-start))
(put 'dired-find-alternate-file 'disabled nil)
