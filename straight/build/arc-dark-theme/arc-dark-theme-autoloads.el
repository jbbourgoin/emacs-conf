;;; arc-dark-theme-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads nil "arc-dark-theme" "arc-dark-theme.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from arc-dark-theme.el

(and load-file-name (boundp 'custom-theme-load-path) (add-to-list 'custom-theme-load-path (file-name-as-directory (file-name-directory load-file-name))))

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "arc-dark-theme" '("arc-dark")))

;;;***

(provide 'arc-dark-theme-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; arc-dark-theme-autoloads.el ends here
